import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="evalpy",
    version="0.0.1",
    author="Alexander Mai",
    author_email="alexandertmai@gmail.com",
    description="A package for evaluting algorithms and getting a pr curve",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/half-potato/evalpy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD 3-Clause License",
        "Operating System :: OS Independent",
    ],
)
