import setuptools

from . import pr, colors
from .pr import *
from .colors import *

name = "eval"
__all__ = ["pr", "colors"]
__all__ += pr.__all__
__all__ += colors.__all__
