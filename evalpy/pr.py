import cv2, math
import numpy as np
from icecream import ic
import matplotlib.pyplot as plt

__all__ = ["imagewise", "pairwise", "saveplot"]

# dims: (images x images x (score, ground_truth))
# lower is better for neural net, greater is better for dbow2
# ground truth is 1 for pos, 0 for neg, 2 for unusable

def imagewise(results, higher_is_better, top_n=100):
    neg = results[:,:,1] == 0
    pos = results[:,:,1] == 1
    print("Num pos: %i" % len(np.where(results[:,:,1] == 1)[0]))
    print("Num neg: %i" % len(np.where(results[:,:,1] == 0)[0]))

    precision, recall = {}, {}
    for i in range(1, top_n):
        precision[i] = []
        recall[i] = []

    for col in range(results.shape[1]):
        # Sort each column
        # filter out unused pairs
        filtered = results[np.where(results[:, col, 1] != 2), col, :][0]
        # filtered = np.squeeze(results[np.where(results[:, col, 1] != 2), col, :])
        # filtered = np.squeeze(results[np.where(results[:, col, 1] != 2), col, :])
        correct_exists = np.count_nonzero((filtered[:, 1] == 1).astype(int)) - 1
        sort_mul = -1 if higher_is_better else 1
        order = np.argsort(sort_mul*filtered[:, 0])
        sortedres = filtered[order, :]
        if correct_exists == 0:
            continue
        for top_n in range(1, top_n):
            # filter out unusable images
            correct_exists_top = np.count_nonzero((sortedres[:top_n, 1] == 1).astype(int)) - 1
            p = correct_exists_top / float(top_n)
            r = correct_exists_top / float(correct_exists)
            precision[top_n].append(p)
            recall[top_n].append(r)
            if r == 1:
                break

    ps, rs = [], []
    for key in precision:
        ps.append(np.mean(precision[key]))
        rs.append(np.mean(recall[key]))
    return ps, rs


def pairwise(results, higher_is_better=True): 
    neg = results[:,:,1] == 0
    pos = results[:,:,1] == 1
    print("Num pos: %i" % len(np.where(results[:,:,1] == 1)[0]))
    print("Num neg: %i" % len(np.where(results[:,:,1] == 0)[0]))
    print("HI")

    ps, rs = [], []
    for t in np.linspace(0, 1, 100):
        threshold = t**2

        if higher_is_better:
            pos_hat = results[:,:,0] > threshold
            neg_hat = results[:,:,0] < threshold
        else:
            pos_hat = results[:,:,0] < threshold
            neg_hat = results[:,:,0] > threshold

        false_pos = len(np.where(np.logical_and(pos_hat, neg))[0])
        true_pos  = len(np.where(np.logical_and(pos_hat, pos))[0]) - 1
        false_neg = len(np.where(np.logical_and(neg_hat, pos))[0])
        true_neg  = len(np.where(np.logical_and(neg_hat, neg))[0])

        precision = float(true_pos) / max(true_pos + false_pos, 1)
        recall = float(true_pos) / max(true_pos + false_neg, 1)
        ic(true_pos, true_neg, false_pos, false_neg, precision, recall)
        if (precision == 0):
            break

        ps.append(precision)
        rs.append(recall)

    return ps, rs

def saveplot(ps, rs, name, path):
    plt.clf()
    plt.title(name)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim(0, 1)
    plt.xlim(0, 1)

    plt.scatter(rs, ps)
    plt.savefig(path)
