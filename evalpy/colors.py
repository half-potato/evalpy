from matplotlib import colors as mcolors

__all__ = ["get_color", "create_color_palette"]

COLORS = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
BY_HSV = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name)
                for name, color in COLORS.items())
MIN_SAT = 100./255
MIN_HUE = 0
min_val = 150./255
HSV_FILT = filter(lambda x: x[0][0] > MIN_HUE and x[0][1] > MIN_SAT and x[0][2] > min_val, BY_HSV)
SORTED_NAMES = [name for hsv, name in HSV_FILT]

def get_color(i):
    return COLORS[SORTED_NAMES[int(i)%len(SORTED_NAMES)]]

def create_color_palette(n, min_sat=100./255):
    hue_di = (1-MIN_HUE)/(n+1)/2
    last_hue = MIN_HUE
    cols = []
    for (hsv, lab) in BY_HSV:
        if hsv[0] - last_hue > hue_di and lab in SORTED_NAMES:
            cols.append(lab)
            last_hue = hsv[0]

    return cols
