import matplotlib as mpl
mpl.use('Agg')
import evalpy
import cv2
import numpy as np
import os
from icecream import ic
import matplotlib.pyplot as plt
import math
import sys
import json
import argparse

linestyles = {
        "resnet50+depth Stanford 2D-3D-S": (":", "r"),
        "resnet50+depth MIT Stata": (":", "b"),
        "resnet50 Stanford 2D-3D-S": ("-", "r"),
        "resnet50 Stanford norm": ("-", "g"),
        "FPSN": ("-", "black"),
        "DBoW2": ("-", "b"),
        "netvlad": ("-.", "r"),
        "netvlad pitts": ("-.", "g"),
}

# this determines what nets are plotted

result_folder = "pr_graphs"
if not os.path.isdir(result_folder):
    os.makedirs(result_folder)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("data", default="stanford.json", help="Json source for where to find gt and results")
    parser.add_argument("networks", nargs="+", help="Which network to test. Write all to get all of them")

    args = parser.parse_args()

    with open(args.data, "r") as f:
        datasets = json.load(f)

    if args.networks[0] == "all":
        net_names = set()
        for ds in datasets:
            net_names.add(ds["net_name"])
        args.networks = net_names

    plot_pr(datasets, args.networks)


def plot_pr(datasets, networks, set_names=set()):
    # palette = evalpy.create_color_palette(len(datasets)//2, min_sat=200./255)

    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim(0, 1)
    plt.xlim(0, 1)
    if len(set_names) == 0:
        for ds in datasets:
            set_names.add(ds["set_name"])

    for set_name in set_names:
        def plot(ps, rs, set_name, net_name, index, legend_refs):
            # plt = plt.subplot(height, width, index+1)
            # plt = plt.subplot()
            # row = index // width
            # col = index % width
            # if row == width-1:
                # plt.set_xlabel("Recall")
            # if col == 0:
                # plt.set_ylabel("Precision")

            # plt.scatter(rs, ps)
            style = linestyles[net_name]
            legend_refs[net_name], = plt.plot(rs[:-1], ps[:-1], c=style[1], linestyle=style[0], lw=2, label=net_name)
        legend_refs = {}
        plt.ylim(0, 1)
        plt.xlim(0, 1)
        plt.xlabel("Recall")
        plt.ylabel("Precision")
        for i, ds in enumerate(datasets):
            if "ignore" in ds and ds["ignore"]:
                continue
            if ds["set_name"] != set_name:
                continue
            if ds["net_name"] not in networks:
                continue
            # Set defaults
            if not "alternating_stereo" in ds:
                ds["alternating_stereo"] = False

            if not "higher_is_better" in ds:
                ds["higher_is_better"] = True
            if not "depth" in ds:
                ds["depth"] = False

            name = "{}_{}".format(ds["set_name"], ds["net_name"])
            print("Loading {}".format(name))
            
            results = cv2.imread(ds["results"], -1)
            gt = cv2.imread(ds["gt"], -1)
            print("Loaded gt from {}".format(ds["gt"]))

            # if ds["net_name"] == "DBoW2":
                # for ref in datasets:
                    # if ref["set_name"] != ds["set_name"]:
                        # continue
                    # if ref["net_name"] == "resnet50":
                        # gt = cv2.imread(ref["results"], -1)[:,:,2]
                        # break
            # else:
                # gt = results[:,:,2]

            if gt is None:
                print("Failed to load ground truth for %s at %s" % (name, ds["gt"]))
                continue
            if results is None:
                print("Failed to load results for {} at {}".format(name, ds["results"]))
                continue

            if len(gt.shape) == 3:
                gt = gt[:,:,0]

            ic(gt.shape, results.shape)
            if gt.shape != results.shape[:2]:
                gt_h = gt.shape[0]
                rs_h = results.shape[0]
                if gt_h - rs_h == 1:
                    gt = gt[:-1, :-1]
                elif rs_h - gt_h == 1:
                    results = results[:-1, :-1]
                elif not ds["alternating_stereo"]:
                    print("%s GT dimensions do not match result dimensions. If the datset is stereo, enable alternating_stereo" % name)
                    continue
                else:
                    # Fix results by taking adding left + right
                    h = results.shape[0]
                    if h % 2 == 1:
                        h -= 1
                    left = results[:h-1:2, :h-1:2]
                    right = results[1::2, 1::2]

                    out = np.zeros_like(left)
                    out[:,:,0] = (left[:,:,0] + right[:,:,0])/2
                    results = out
                    # tmp = np.where(left[:,:,0] > right[:,:,0])
                    # out[tmp, 0] = left[tmp, 0]
                    # tmp = np.where(left[:,:,0] < right[:,:,0])
                    # out[tmp, 0] = right[tmp, 0]
                    # results = out

            if np.max(gt) == 255:
                gt[np.where(gt == 255)] = 1

            ind = np.tri(gt.shape[0], gt.shape[0], -1)
            gt = gt*ind + gt.T*ind.T
            np.fill_diagonal(gt, 1)
            if ds["higher_is_better"]:
                np.fill_diagonal(results[:,:,0], 1)
            else:
                np.fill_diagonal(results[:,:,0], 0)

            # gt[np.where(gt > 1)] = 1

            results[:,:,1] = gt
            results[:,:,2] = gt

            # print(gt)
            # cv2.imshow("GT", gt*255)
            # cv2.imshow("Results", results[:,:,0])
            # for i in range(10):
                # cv2.imshow("Res", results[:,:,0] * (results[:,:,0] > i/10))
                # cv2.waitKey(0)

            # ps1, rs1 = evalpy.imagewise(results, ds["higher_is_better"])
            # plt_name = name+"_imgwise"
            # evalpy.saveplot(ps1, rs1, plt_name, os.path.join(result_folder, plt_name+".png"))
            # ps2, rs2 = evalpy.pairwise(results, ds["higher_is_better"])
            ps2, rs2 = pairwise(results, ds["higher_is_better"])
            plt_name = "{}_pairwise".format(name)
            plot(ps2, rs2, ds["set_name"], ds["net_name"], i, legend_refs)
            # evalpy.saveplot(ps2, rs2, plt_name, os.path.join(result_folder, plt_name+".png"))
        # l1 = plt.legend(loc=3)
        # LEGEND_LABELS = ["DBoW2", "resnet50", "resnet50+depth", "resnet50+depth mit", "FPSN"]
        keys = [net for net in networks if net in legend_refs]
        vals = [legend_refs[net] for net in networks if net in legend_refs]
        l2 = plt.legend(vals, keys, loc=3)
        # plt.gca().add_artist(l1)
        plt.gca().add_artist(l2)
        plt.title(set_name)
        # plt.show()
        plt.savefig(os.path.join(result_folder, "{}.svg".format(set_name)))
        plt.savefig(os.path.join(result_folder, "{}.png".format(set_name)))
        plt.clf()

def pairwise(results, higher_is_better=True):
    neg = results[:,:,1] == 0
    pos = results[:,:,1] == 1
    print("Num pos: %i" % len(np.where(results[:,:,1] == 1)[0]))
    print("Num neg: %i" % len(np.where(results[:,:,1] == 0)[0]))

    ps, rs = [], []
    u = np.mean(results[:,:,0])
    s = np.std(results[:,:,0])
    # for t in np.linspace(max(0, u-2*s), u+2*s, 10):
    for t in np.linspace(np.min(results[:,:,0]), np.max(results[:,:,0]), 20)[1:]:
        threshold = t**2

        if higher_is_better:
            pos_hat = results[:,:,0] > threshold
            neg_hat = results[:,:,0] < threshold
        else:
            pos_hat = results[:,:,0] < threshold
            neg_hat = results[:,:,0] > threshold

        false_pos = len(np.where(np.logical_and(pos_hat, neg))[0])
        true_pos  = len(np.where(np.logical_and(pos_hat, pos))[0]) - 1
        false_neg = len(np.where(np.logical_and(neg_hat, pos))[0])
        true_neg  = len(np.where(np.logical_and(neg_hat, neg))[0])

        precision = float(true_pos) / max(true_pos + false_pos, 1)
        recall = float(true_pos) / max(true_pos + false_neg, 1)
        ic(true_pos, true_neg, false_pos, false_neg, precision, recall)
        if (precision == 0):
            break

        ps.append(precision)
        rs.append(recall)

    return ps, rs

if __name__ == "__main__":
    main()
